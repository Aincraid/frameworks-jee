package com.isgeic3.gsn.GestionBanque.resources.webController;


import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.domain.Operation;
import com.isgeic3.gsn.GestionBanque.exception.CompteIntrouvableException;
import com.isgeic3.gsn.GestionBanque.repository.CompteJpaRepository;
import com.isgeic3.gsn.GestionBanque.services.CompteService;
import com.isgeic3.gsn.GestionBanque.services.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CompteControllerWeb {
    @Autowired
    private CompteJpaRepository compteJpaRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private OperationService operationService;

    @RequestMapping("/comptes")
    public String index(){
        return "comptes";
    }

    @RequestMapping(value = "/consulter", method = RequestMethod.GET)
    public String consulter(Model model,@RequestParam("Compte") String numCompte,@RequestParam(name = "page",defaultValue = "0") int pages,
                            @RequestParam(name = "size",defaultValue = "5")int size) throws CompteIntrouvableException {
       try {
           Compte compte = compteService.consulter(Long.valueOf(numCompte));
           model.addAttribute("compte", compte);
           model.addAttribute("numcompte", numCompte);
           Page<Operation> operationPage = operationService.ListOp(Long.valueOf(numCompte), pages, size);
           model.addAttribute("listOp", operationPage.getContent());
           int [] page = new int[operationPage.getTotalPages()];
           model.addAttribute("pages", page);
       }catch (Exception e){
           model.addAttribute("exception",e);
       }
        return "comptes";
    }
}

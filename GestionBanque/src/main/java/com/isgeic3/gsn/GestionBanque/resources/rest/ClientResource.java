package com.isgeic3.gsn.GestionBanque.resources.rest;


import com.isgeic3.gsn.GestionBanque.domain.Client;
import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.resources.dto.ClientDto;
import com.isgeic3.gsn.GestionBanque.resources.dto.CompteDto;
import com.isgeic3.gsn.GestionBanque.services.ClientService;
import com.isgeic3.gsn.GestionBanque.services.CompteService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/client")
@Slf4j
@AllArgsConstructor
public class ClientResource {

    private final ClientService clientService;
    private final CompteService compteService;

    @PostMapping(value = "/register")
    ResponseEntity<ClientDto> register(@RequestBody ClientDto clientDto) {

        Client client = mapToclient(clientDto);

        return new ResponseEntity<>(mapToClientDto(clientService.register(client)), HttpStatus.OK);
    }

    @GetMapping(value = "/findAll")
    ResponseEntity<List<ClientDto>> findAll(){
        List<ClientDto> ClientDtos = new ArrayList<>();
        List<Client> Clients = clientService.findAllClient();
        for (Client Client: Clients){
            ClientDtos.add(mapToClientDto(Client));
        }
        return new ResponseEntity<>(ClientDtos, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteClient(@PathVariable Long id){
        clientService.deleteById(id);
    }

    private Client mapToclient(ClientDto clientDto) {
        Client client = new Client();
        client.setNom(clientDto.getNom());
        client.setPrenom(clientDto.getPrenom());
        client.setNum(clientDto.getNum());
        client.setEmail(clientDto.getEmail());
        client.setNaissance(clientDto.getNaissance());
        client.setNationalite(clientDto.getNationalite());
        return client;
    }

    private ClientDto mapToClientDto(Client client){
        ClientDto dto = new ClientDto();
       /* dto.setId(client.getId());*/
        dto.setNom(client.getNom());
        dto.setPrenom(client.getPrenom());
        dto.setNum(client.getNum());
        dto.setEmail(client.getEmail());
        dto.setNaissance(client.getNaissance());
        dto.setNationalite(client.getNationalite());
        return dto;
    }
    private CompteDto mapToCompteDto(Compte compte){
        CompteDto dto = new CompteDto();
        /*  dto.setId(compte.getId());*/
        /*dto.setClient(compte.getClient());
        dto.setMdp(compte.getMdp());*/
        dto.setNumCompte(compte.getNumCompte());
        dto.setSolde(compte.getSolde());
        return dto;
    }

}

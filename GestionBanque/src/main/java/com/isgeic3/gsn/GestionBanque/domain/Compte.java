package com.isgeic3.gsn.GestionBanque.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "Comtpe")
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"numCompte", "mdp"})
@Getter
@Setter
@Data
@AllArgsConstructor
public class Compte {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long numCompte;
    private String mdp;
    private double solde;

    @ManyToOne
    private Client client;

    @OneToMany(mappedBy = "compte")
    private Collection<Operation> operations;

    public Compte(Long numCompte, String mdp, double solde, Client client) {
        this.numCompte = numCompte;
        this.mdp = mdp;
        this.solde = solde;
        this.client = client;
    }
}

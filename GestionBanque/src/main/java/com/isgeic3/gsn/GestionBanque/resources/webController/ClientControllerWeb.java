package com.isgeic3.gsn.GestionBanque.resources.webController;

import com.isgeic3.gsn.GestionBanque.domain.Client;
import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.repository.ClientJpaRepository;
import com.isgeic3.gsn.GestionBanque.repository.CompteJpaRepository;
import com.isgeic3.gsn.GestionBanque.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

@Controller
public class ClientControllerWeb {
    @Autowired
    private ClientJpaRepository clientJpaRepository;
    @Autowired
    private ClientService clientService;
    @Autowired
    private CompteJpaRepository compteJpaRepository;

    @RequestMapping(value="/nouveau")
    String creerCompte() {
        return "creerCompte";
    }

    @RequestMapping(value="/create",method= RequestMethod.POST)
     String createNewAccount(String numCompte, String nom,String prenom, Long num,  String email, String naissance,
                                   String nationalite, double solde,String mdp) {

        if (numCompte.length()!=6 || solde!=100000){
            throw new RuntimeException("Numero de compte incorrect ou Solde incorrect");
        }else {
            Client client=clientJpaRepository.save(new Client(nom,prenom,num,email,naissance,nationalite));
            compteJpaRepository.save(new Compte(Long.valueOf(numCompte),mdp,solde,client));

        }
        return "redirect:/comptes";
    }

}

package com.isgeic3.gsn.GestionBanque.services;


import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.domain.Operation;
import com.isgeic3.gsn.GestionBanque.exception.CompteIntrouvableException;
import com.isgeic3.gsn.GestionBanque.exception.SoldeInsuffisantException;
import com.isgeic3.gsn.GestionBanque.repository.CompteJpaRepository;
import com.isgeic3.gsn.GestionBanque.repository.OperationJpaRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Slf4j
@AllArgsConstructor
@Transactional
public class OperationService {
    private CompteJpaRepository compteJpaRepository;
    private OperationJpaRepository operationJpaRepository;
    private final CompteService compteService;

    public Operation virement(Long numCompte, Long numCompte2, double montant) throws CompteIntrouvableException, SoldeInsuffisantException {
        Compte compte1 = compteService.consulter(numCompte);
        Compte compte2 = compteService.consulter(numCompte2);
        if (compte1.getSolde() < montant){
            throw new SoldeInsuffisantException("Solde Insuffisant");
        }else {

            compte1.setSolde(compte1.getSolde() - montant);
            compte2.setSolde(compte2.getSolde() + montant);
            compteJpaRepository.save(compte1);
            compteJpaRepository.save(compte2);
            Operation operation=operationJpaRepository.save(new Operation(montant, new Date(), compte1));
            return operation;
        }
    }

    public Page<Operation> ListOp(Long numComtpe,int page, int size){
        return  operationJpaRepository.ListOpe(numComtpe, PageRequest.of(page, size));
    }

    }


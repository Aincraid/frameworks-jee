package com.isgeic3.gsn.GestionBanque.repository;

import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.domain.Operation;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface CompteJpaRepository extends JpaRepository<Compte, Long> {

    @Transactional
    public Compte findByNumCompte(Long numCompte);
    /*@Transactional
    public Page<Operation> ListOp(Long numCompte, int page, int size);*/
}

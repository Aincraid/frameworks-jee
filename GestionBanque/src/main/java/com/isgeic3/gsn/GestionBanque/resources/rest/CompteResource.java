package com.isgeic3.gsn.GestionBanque.resources.rest;


import com.isgeic3.gsn.GestionBanque.domain.Client;
import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.exception.CompteIntrouvableException;
import com.isgeic3.gsn.GestionBanque.resources.dto.CompteDto;
import com.isgeic3.gsn.GestionBanque.resources.dto.CompteDto;
import com.isgeic3.gsn.GestionBanque.services.CompteService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/compte")
@Slf4j
@AllArgsConstructor
public class CompteResource {

    @Autowired
    private final CompteService compteService;

    @GetMapping(value = "/consulter")
    ResponseEntity<CompteDto> consulterCpte(@RequestParam Long numCpte) throws CompteIntrouvableException {
        Compte compte = compteService.consulter(numCpte);
        return new ResponseEntity<>(mapToCompteDto(compte), HttpStatus.OK);

    }

    @PostMapping(value = "/register")
    ResponseEntity<CompteDto> register(@RequestBody CompteDto compteDto){
        Compte compte = mapToCompte(compteDto);
        return new ResponseEntity<>(mapToCompteDto(compte),HttpStatus.OK);

    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteClient(@PathVariable Long id){
        compteService.deleteById(id);
    }



   private Compte mapToCompte(CompteDto compteDto) {
        Compte compte = new Compte();
        compte.setNumCompte(compteDto.getNumCompte());
        compte.setMdp(compte.getMdp());
        compte.setSolde(compteDto.getSolde());
        compte.setClient(compte.getClient());
        return compte;
    }

    private CompteDto mapToCompteDto(Compte compte){
        CompteDto dto = new CompteDto();
      dto.setNumCompte(dto.getNumCompte());
      dto.setSolde(dto.getSolde());
      dto.setClient(dto.getClient());
        return dto;
    }
}

package com.isgeic3.gsn.GestionBanque.repository;

import com.isgeic3.gsn.GestionBanque.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientJpaRepository extends JpaRepository<Client, Long> {


}

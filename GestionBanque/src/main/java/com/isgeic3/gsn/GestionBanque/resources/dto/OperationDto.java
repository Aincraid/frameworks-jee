package com.isgeic3.gsn.GestionBanque.resources.dto;

import com.isgeic3.gsn.GestionBanque.domain.Compte;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Getter
@Setter
public class OperationDto {

    private double montant;
    private Date dateOp;

    @ManyToOne
    private Compte compte;
}

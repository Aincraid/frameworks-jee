package com.isgeic3.gsn.GestionBanque.domain;


import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Operation")
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"dateOp", "montant"})
@Getter
@Setter
@Data
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double montant;
    private Date dateOp;

    @ManyToOne
    private Compte compte;

    public Operation(double montant, Date date,  Compte compte1) {
        this.montant = montant;
        this.dateOp=date;
        this.compte=compte1;
        compte1.getId();


    }
}

package com.isgeic3.gsn.GestionBanque.domain;


import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Client")
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"nom", "prenom"})
@Getter
@Setter
@Data
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String prenom;
    private Long num;
    private String email;
    private String naissance;
    private String nationalite;

    @OneToMany(mappedBy = "client")
    private Collection<Compte> comptes;


    public Client(String nom, String prenom, Long num, String email, String naissance, String nationalite) {
        this.nom = nom;
        this.prenom = prenom;
        this.num = num;
        this.email = email;
        this.naissance = naissance;
        this.nationalite = nationalite;
    }
}

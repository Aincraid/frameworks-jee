package com.isgeic3.gsn.GestionBanque.resources.rest;


import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.domain.Operation;
import com.isgeic3.gsn.GestionBanque.exception.CompteIntrouvableException;
import com.isgeic3.gsn.GestionBanque.exception.SoldeInsuffisantException;
import com.isgeic3.gsn.GestionBanque.resources.dto.CompteDto;
import com.isgeic3.gsn.GestionBanque.resources.dto.OperationDto;
import com.isgeic3.gsn.GestionBanque.services.OperationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/operation")
@Slf4j
@AllArgsConstructor
public class OperationResource {
    @Autowired
    private final OperationService operationService;

    @PostMapping(value = "/virement")
    ResponseEntity<OperationDto> transfert(@RequestParam Long numCompte1, Long numCompte2, Long montant)throws CompteIntrouvableException, SoldeInsuffisantException{
        Operation operation = operationService.virement(numCompte1, numCompte2, montant);
        return new ResponseEntity<>(mapToOperationDto(operation), HttpStatus.OK);
    }


    private OperationDto mapToOperationDto(Operation operation){
        OperationDto dto = new OperationDto();
        dto.setDateOp(operation.getDateOp());
        dto.setMontant(operation.getMontant());
        return dto;
    }
}

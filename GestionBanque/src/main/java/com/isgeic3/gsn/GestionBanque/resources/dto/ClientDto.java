package com.isgeic3.gsn.GestionBanque.resources.dto;


import com.isgeic3.gsn.GestionBanque.domain.Compte;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ClientDto {

    private String nom;
    private String prenom;
    private Long num;
    private String email;
    private String naissance;
    private String nationalite;


}

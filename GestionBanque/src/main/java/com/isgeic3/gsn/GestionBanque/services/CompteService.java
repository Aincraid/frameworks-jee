package com.isgeic3.gsn.GestionBanque.services;


import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.domain.Operation;
import com.isgeic3.gsn.GestionBanque.exception.CompteIntrouvableException;
import com.isgeic3.gsn.GestionBanque.exception.SoldeInsuffisantException;
import com.isgeic3.gsn.GestionBanque.repository.ClientJpaRepository;
import com.isgeic3.gsn.GestionBanque.repository.CompteJpaRepository;
import com.isgeic3.gsn.GestionBanque.repository.OperationJpaRepository;
import com.isgeic3.gsn.GestionBanque.resources.dto.CompteDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class CompteService {

    private final CompteJpaRepository compteJpaRepository;
    private final OperationJpaRepository operationJpaRepository;

    public Compte consulter(Long numCpte) throws CompteIntrouvableException {
        Compte compte = compteJpaRepository.findByNumCompte(numCpte);
        if(compte.equals(null)){
            throw new CompteIntrouvableException("Compte Introuvable");
        }
        return compte;
    }

    public void deleteById(Long id){
        compteJpaRepository.deleteById(id);
    }



}

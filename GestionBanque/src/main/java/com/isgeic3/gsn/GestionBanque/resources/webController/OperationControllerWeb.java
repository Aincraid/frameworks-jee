package com.isgeic3.gsn.GestionBanque.resources.webController;

import com.isgeic3.gsn.GestionBanque.domain.Operation;
import com.isgeic3.gsn.GestionBanque.exception.CompteIntrouvableException;
import com.isgeic3.gsn.GestionBanque.repository.OperationJpaRepository;
import com.isgeic3.gsn.GestionBanque.services.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class OperationControllerWeb {
    @Autowired
    private OperationJpaRepository operationJpaRepository;
    @Autowired
    private OperationService operationService;

    @RequestMapping(value = "/saveOp", method = RequestMethod.POST)
    public String saveOperation(Model model, @RequestParam("Compte") String numCompte, String numCompte2, double montant){
        try {
            if (numCompte.equals(numCompte2)){
                throw new RuntimeException("Virement Impossible sur le meme compte");
            }else {
                operationService.virement(Long.valueOf(numCompte), Long.valueOf(numCompte2), montant);
                model.addAttribute("numCompte", numCompte);
                model.addAttribute("numCompte2", numCompte2);
                model.addAttribute("montant",montant);
            }
        } catch (Exception e){
            model.addAttribute("error", e);
            return "redirect:/consulter?Compte="+numCompte + "&error"+e;
        }

        return "redirect:/consulter?Compte="+numCompte;
    }

}

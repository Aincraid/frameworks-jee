package com.isgeic3.gsn.GestionBanque.services;


import com.isgeic3.gsn.GestionBanque.domain.Client;
import com.isgeic3.gsn.GestionBanque.domain.Compte;
import com.isgeic3.gsn.GestionBanque.repository.ClientJpaRepository;
import com.isgeic3.gsn.GestionBanque.repository.CompteJpaRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ClientService {

    private final ClientJpaRepository clientJpaRepository;
    private final CompteJpaRepository compteJpaRepository;

    public Client register(Client client){

        return clientJpaRepository.save(client);
    }

    public List<Client> findAllClient(){
        return clientJpaRepository.findAll();
    }

    public void deleteById(Long id){
        clientJpaRepository.deleteById(id);
    }
}

package com.isgeic3.gsn.GestionBanque.repository;

import com.isgeic3.gsn.GestionBanque.domain.Operation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface OperationJpaRepository extends JpaRepository<Operation, Long> {

    /*@Transactional
    public void virement(Long numCompte1, Long numCompte2, Long montant);*/
    @Transactional
    @Query("select o from Operation o where o.compte.numCompte=:x order by o.dateOp desc")
    public Page<Operation> ListOpe(@Param("x") Long numCompte, Pageable pageable);
}

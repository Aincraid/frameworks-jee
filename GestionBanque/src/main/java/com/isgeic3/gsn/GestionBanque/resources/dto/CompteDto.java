package com.isgeic3.gsn.GestionBanque.resources.dto;


import com.isgeic3.gsn.GestionBanque.domain.Client;
import com.isgeic3.gsn.GestionBanque.domain.Operation;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Collection;

@Getter
@Setter
public class CompteDto {

    private Long numCompte;
   /* private String mdp;*/
    private double solde;
    @ManyToOne
    private Client client;

}

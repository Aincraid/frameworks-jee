# Auteur :  OUEDRAOGO ERIC ET NIKIEMA SERGE								 
# Date : 16 / 01 / 2022										 
# Lieu : ISGE-BF/IC3										 

# Projet :
Une application web de gestion des comptes bancaires en Java/ JEE et Spring.	

						 
# Spécificités fonctionnelles :
- Chaque compte appaertient à un client.
- Un client peut avoir un ou plusieurs compte
- L'application doit permettre de :

    0. Crée un compte
    1. Consulter le compte d'un client
	2.Effectuer un virement d'un compte vers un autre compte. Il faut verifier le montant pour le virement qu'il ne depasse pas le solde disponible
    2. Consulter les opérations d'un compte 

# Specificité techniques : 
- Pour la consultation des opération, les opérations doivent
 s'afficher dans des pages. On doit utiliser la pagination, c'est à dire qu'on ne veut pas afficher
 toutes les opérationd sur la meme page. 
- L'application doit être sécurisée. Seuls les personnes identifiées qui sont autorisées à 
effectuer les opérations ci-dessus mentionnées.(Securisation non effectuer)
- L'application doit gérer les erreurs liées à l'utilisateur

# Architeure de l'application 
- L'application est basée sur une architecutre en couche à savoir :
 1. Une couche JpaRepository pour l'accès aux données. Ici on crée les interfaces ClientJpaRepository,
 CompteJpaRepository et OperationJpaRepository qui heritent de l'interface JpaRepository.
 2. Une couche Entities où on crée nos entités (classes) que seront mappées avec les tables dans la base de données
 3.Une couche Service où on crée les interfaces service qui définit les opérations ci-dessous mentionnées et 
 un package Resource qui implemente ces classes
 4. Une couche webController (présentation) où on gère tout ce qui est web (View)
 
 # Technologies utilisées :
 - JavaEE
 - Spring Boot, Spring Data JPA, Spring Security, SpringDoc(openApi),Lombok, DevTools, Spring Web, Spring Data Rest
 - View (UI) : Thymeleaf, JavaScript, Bootstrap
 - SGBD : MySQL
 
 # Outis de développement: 
 - IDE : intelliJ
 - Gestion des dépendences : Maven

# Comment utiliser cette application ?
Pour utiliser cette application, il suffit de modifier le fichier application.properties pour changer 
le nom de la base de donnée.
La page d'acceuil est http://localhost:1000/comptes
Pour creer un client aller sur l'onglet client


